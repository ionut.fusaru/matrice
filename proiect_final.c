#include <stdio.h>
#include <time.h>

void generare_aleatoare_matrice(int m[][20], int l, int c)
{
	int i, j;
	srand(time(0));
	for (i = 0; i < l; i++)
		for (j = 0; j < c; j++)
			m[i][j] = rand() % 1000;
}

void afisare_matrice(int m[][20], int l, int c)
{
	int i, j;
	printf("\n");
	for (i = 0; i < l; i++)
	{
		for (j = 0; j < c; j++)
			printf("%5d", m[i][j]);
		printf("\n");
	}
	printf("\n");
}

int matrice_patratica(int m[][20], int l, int c)
{
	if (l == c)
		return 1;
	else
		return 0;
}

void suma_diagonale(int m[][20], int l, int c)
{
	int i, j;
	int Sp = 0, Ss = 0;
	if (matrice_patratica(m, l, c))
	{
		printf("Avem matrice patratica :)\n");
		for (i = 0; i < l; i++)
			for (j = 0; j < l; j++)
				if (i == j)
					Sp = Sp + m[i][j];
		printf("Suma de pe DP este: %d\n", Sp);
		for (i = 0; i < l; i++)
			for (j = 0; j < l; j++)
				if (i + j == l - 1)
					Ss = Ss + m[i][j];
		printf("Suma de pe DS este: %d\n", Ss);
	}
	else
		printf("Nu e matrice patratica!\n");
	printf("\n");
}

void maxim_sub_diagonale(int ma[][20], int n)
{
	int i, j, max1 = ma[n - 1][n - 1], max2 = 0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			if (i + j > n - 1)
				if (ma[i][j] > max1)
					max1 = ma[i][j];
	}
	printf("Maximul dintre elementele de sub DS este: %d\n", max1);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			if (i > j)
				if (ma[i][j] > max2)
					max2 = ma[i][j];
	}
	printf("Maximul dintre elementele de sub DP este: %d\n", max2);
}

void minim_sub_diagonale(int m[][20], int n)
{
	int i, j, min1 = m[n - 1][0], min2 = m[n - 1][n - 1];
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			if (i > j)
				if (m[i][j] < min1)
					min1 = m[i][j];
	}
	printf("Minimul dintre elementele de sub DP este: %d\n", min1);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			if (i + j > n - 1)
				if (m[i][j] < min2)
					min2 = m[i][j];
	}
	printf("Minimul dintre elementele de sub DS este: %d\n", min2);
}

int main()
{

	int M[20][20];
	int m, n;

	printf("Dati numar linii:");
	scanf("%d", &m);
	printf("Dati numar coloane:");
	scanf("%d", &n);

	generare_aleatoare_matrice(M, m, n);

	afisare_matrice(M, m, n);

	if (matrice_patratica(M, m, n))
	{
		suma_diagonale(M, m, n);

		maxim_sub_diagonale(M, m);

		minim_sub_diagonale(M, m);
	}
	else
		printf("Matrice nepatratica, deci nu exista diagonale!\n");

	return 0;
}